# Portainer for DeMi

This repository contains a maintained `docker compose` file (DeMi "stack") for
[Watchtower](https://containrrr.dev/watchtower/).

Usage:

```shell
./bootstrap             # check dependencies
./config                # write configuration .env
./tasks up              # start container
```

- `bootstrap` this script sets up all dependencies needed to bring up the containers.
- `config` this script writes the `.env` configuration.
- `tasks` Using `tasks up` to start the container will write the reuqired config.json file for authentication against the CERN GitLab registry.


---
Contact: G. Brandt <gbrandt@cern.ch>

